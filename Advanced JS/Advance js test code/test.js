"use strict";

// const { log } = require("grunt");

// const user = {
//   name: "Sasha",
//   age: 25,
//   data: function () {
//     return console.log("Name is " + this.name + " age is " + this.age);
//   },
// };

// const valera = {
//   name: "Valera",
//   age: 23,
// };

// user.data.call(valera);

// const armodroms = [
//   "Отравленный кинжал",
//   "Золотой бог",
//   "Гарганто",
//   "Flamberg",
//   "Knight",
// ];

// const airships = ["boieng", "Embraer", "Airbus"];

// const mergedArray = [...airships, ...armodroms];

// const man = {
//   "first name": "Brian",
//   weapon: "Gun",
//   "Last name": "Oconer",
//   status: "searcher",
//   vid: "something",
// };

// function createWarrior({ "first name": name, status, weapon }) {
//   const warrior = {
//     name,
//     status,
//     weapon,
//   };
//   return warrior;
// }

// const product = {
//   name: "Iphone",
//   model: "12 Pro",
//   company: "Apple",
//   price: 1400,
// };

// const topCompanies = ["Apple", "Samsung", "Xiaomi", "Philips", "Motorolla"];

// const [company1, , company3] = topCompanies;

// const person = "Tom, Myers, tom@gmail.com, 8/7/1980";

// const arr = person.split(",");

// const [name, surname, email, date] = arr;

// const birthdayDate = date.split("/") ;

// class Person {
//   constructor(firstname, lastname) {
//     (this.firstname = firstname), (this.lastname = lastname);
//   }
//   getFullName() {
//     return this.firstname + " " + this.lastname;
//   }
// }

// class User extends Person {
//   constructor(firsname, lastname, email, password) {
//     super(firsname, lastname);
//     this.email = email;
//     this.password = password;
//   }
//   getEmail() {
//     return this.email;
//   }
//   getPassword() {
//     return this.password;
//   }
// }

// let person = new Person("DAN", "Abramow");
// person.getFullName();

// let person1 = new Person("Sasha", "Altys");

// function app() {
//   let user = new User("Dan", "Abramov", "dan@abramov.com", "1234567");
//   user.getFullName(); //> "Dan Abramov"
//   user.getEmail(); //> "dan@abramov.com"
//   user.getPassword(); //> "iLuvES6"
//   user.firstName; //> "Dan"
//   user.lastName; //> "Abramov"
//   user.email; //> "dan@abramov.com"
//   user.password; //> "iLuvES6"
// }

// Array.prototype.spread = function (prep) {
//   let passed = [];
//   let failed = [];
//   for (let i = 0; i < this.length; i++) {
//     if (prep(this[i])) {
//       passed.push(this[i]);
//     } else {
//       failed.push(this[i]);
//     }
//   }
//   return [passed, failed];
// };

// const newArr = [1, 4, 2, 10, 20, 40, 100];

// console.log(newArr.spread((e) => e <= 50));
// console.log(newArr);

// let newArr = [1, 2, 3, 4];
// let foo = [1, 2, 3];

// Array.prototype.sayHi = () => "Hello";

// console.log(newArr.sayHi());
// console.log(foo.sayHi());

// function Person(firstname, lastName) {
//   this.firstname = firstname;
//   this.lastName = lastName;
// }

// Person.prototype.getFullName = function () {
//   return this.firstname + " " + this.lastName;
// };

// let person = new Person("Sasha", "Altys");

// function User(firsname, lastName, email, password) {
//   Person.call(this, firsname, lastName);
//   this.email = email;
//   this.password = password;
// }

// User.prototype = Object.create(Person.prototype);

// User.prototype.setEmail = function (mail) {
//   this.email = mail;
// };

// User.prototype.getEmail = function () {
//   return this.email;
// };

// const user = new User("Sasha", "Altys", "altys77@gmail.com");

// const Customer = {
//   plan: "trial",
// };
// Customer.setPremium = function () {
//   this.plan = "premium";
// };

// user.customer = Customer;

// user.customer.setPremium();

// console.log(user);

// class User {
//   constructor(name) {
//     this.name = name;
//   }
//   sayHi() {
//     alert("hi " + this.name);
//   }
// }

// const person = new User("Sasha");

// person.sayHi();

// class User {
//   constructor(name) {
//     this.name = name;
//   }
//   getName() {
//     return this.name;
//   }
//   setName(value) {
//     if (value.length <= 4) {
//       alert("name should be at least 4 symbols");
//       return;
//     }
//     return (this.name = value);
//   }
// }

// const person = new User("Sasha");

/*
 Заполните колонку Colors цветами параграфов из колонки Paragraphs.
 Для решения задачи позаимствуйте метод "map" у массива.
 */

// const pCollection = document.getElementsByTagName("p");
// const colorsTable = document.querySelector("#colors");

// const pColorCollection = [].map.call(
//   pCollection,
//   (e) => `<p>${e.style.color}</p>`
// );

// colorsTable.insertAdjacentHTML("afterbegin", pColorCollection.join(""));

/*
Допишите логику функций writeAverageGrade, writeMinGrade, writeMaxGrade
которые считают оценки и отрисовывают их в соответсвующие ячейки таблицы.

Имитируя загрузку данных выводите оценки в таблицу (используйте селекторы из объекта) через 2, 4, 6 секунд.
При этом прелоадер нужно удалять.

Используйте bind! :)
 */

// const grades = {
//   selectors: {
//     average: "#average",
//     max: "#max",
//     min: "#min",
//   },
//   list: {
//     math: 10,
//     english: 8,
//     literature: 11,
//     history: 7,
//     biology: 9,
//     programming: 12,
//   },
//   writeAverageGrade() {
//     let gradeArr = Object.values(this.list);
//     const allGrades = gradeArr.reduce((e, acc) => e + acc, 0);
//     document.querySelector(this.selectors.average).innerHTML = `<p>${
//       allGrades / gradeArr.length
//     }</p>`;
//   },
//   writeMinGrade() {
//     const minGrade = Object.values(this.list);
//     const minValue = Math.min(...minGrade);
//     document.querySelector(this.selectors.min).innerHTML = `<p>${minValue}</p>`;
//   },
//   writeMaxGrade() {
//     const maxGrade = Object.values(this.list);
//     const maxValue = Math.min(...maxGrade);
//     document.querySelector(this.selectors.max).innerHTML = `<p>${maxValue}</p>`;
//   },
// };

// // setTimeout(grades.writeMinGrade, 2000);
// // setTimeout(grades.writeMaxGrade, 4000);
// // setTimeout(grades.writeAverageGrade, 6000);

// setTimeout(grades.writeAverageGrade.bind(grades), 6000);
// setTimeout(grades.writeMinGrade.bind(grades), 2000);
// setTimeout(grades.writeMaxGrade.bind(grades), 4000);
// const someText =
//   'Модель наследования в JavaScript может озадачить опытных разработчиков на высокоуровневых объектно-ориентированных языках (таких, например, как Java или C++), поскольку она динамическая и не включает в себя реализацию понятия class (хотя ключевое слово class, бывшее долгие годы зарезервированным, и приобрело практическое значение в стандарте ES2015, однако, Class в JavaScript ES>=6 представляет собой лишь "синтаксический сахар" поверх прототипно-ориентированной модели наследования).\n' +
//   "\n" +
//   "В плане наследования JavaScript работает лишь с одной сущностью: объектами. Каждый объект имеет внутреннюю ссылку на другой объект, называемый его прототипом. У объекта-прототипа также есть свой собственный прототип и так далее до тех пор, пока цепочка не завершится объектом, у которого свойство prototype равно null.  По определению, null не имеет прототипа и является завершающим звеном в цепочке прототипов.\n" +
//   "\n" +
//   "Хотя прототипную модель наследования некоторые относят к недостаткам JavaScript, на самом деле она мощнее классической. К примеру, поверх неё можно предельно просто реализовать классическое наследование, а вот попытки совершить обратное непременно вынудят вас попотеть.";

// function ShowMore(text, numberOfLines, containerSelector = "body") {
//   this.text = text;
//   this.numberOfLines = numberOfLines;
//   this.containerSelector = containerSelector;
//   this.span = document.createElement("span");
//   this.button = document.createElement("button");
// }

// ShowMore.prototype.render = function () {
//   const container = document.querySelector(this.containerSelector);
//   const wrapper = document.createElement("div");
//   wrapper.classList.add("show-more__wrapper");
//   const span = document.createElement("span");
//   span.classList.add("show-more__text");
//   const button = document.createElement("button");
//   button.classList.add("show-more__button");
//   span.innerText = this.text;
//   button.innerText = "Show More";
//   container.append(wrapper);
//   wrapper.append(span);
//   wrapper.append(button);
// };

// const showSomeText = new ShowMore(someText, 3, ".container");
// showSomeText.render();

// class Box extends ShowHide {
//   constructor(options) {
//     super(options.selector);
//     this.$el.style.width = options.width + "px";
//     this.$el.style.height = options.height + "px";
//     this.$el.style.backgroundColor = options.background;
//   }
// }

// const box1 = new Box({
//   selector: "#block1",
//   width: 200,
//   height: 200,
//   background: "red",
// });

// class Circle extends Box {
//   constructor(options) {
//     super(options);
//     this.$el.style.borderRadius = "50%";
//   }
// }

// const circle = new Circle({
//   selector: "#circle",
//   width: 300,
//   height: 300,
//   background: "green",
// });

// class Figure {
//   constructor(selector) {
//     this.$el = document.querySelector(selector);
//   }
// }

// class FigureSettings extends Figure {
//   constructor(options) {
//     super(options.selector);
//     this.$el.style.width = options.width + "px";
//     this.$el.style.height = options.height + "px";
//     this.$el.style.backgroundColor = options.color;
//   }
// }

// const createFigure = new FigureSettings({
//   selector: "#block",
//   width: 100,
//   height: 100,
//   color: "red",
// });

// class Check {
//   constructor(selector) {
//     this.$el = document.body.querySelector(selector);
//     console.log($el);
//     console.log("work");
//   }
// }

// const check = new Check({ selector: "#block" });

// class Car {
//   constructor(maker, model) {
//     this.maker = maker;
//     this.model = model;
//   }
//   drive() {
//     console.log("Zoom");
//   }
// }

// class Tesla extends Car {
//   constructor(model, chargetime) {
//     super("Tesla", model);
//     this.chargetime = chargetime;
//   }
//   charge() {
//     console.log("Chraging...");
//   }
// }

// const tesla = new Tesla("3", 20);

// function Car(maker, model) {
//   this.maker = maker;
//   this.model = model;
// }

// Car.prototype.drive = function () {
//   console.log("Driving");
// };

// const tesla = new Car("tesla", "3");

// function Dz(model, chargeTime) {
//   Car.call(this, "Dziguli", model);
//   this.chargeTime = chargeTime;
// }
// Dz.prototype = Object.create(Car.prototype);
// Object.defineProperty(Dz.prototype, "constructor", {
//   value: Dz,
// });

// const dz = new Dz("3", "Charging");

// class Car {
//   constructor(mark, model) {
//     this.mark = mark;
//     this.model = model;
//   }
//   charge() {
//     console.log("Charging");
//   }
// }

// class Dz extends Car {
//   constructor(year, chargeTime) {
//     super("Dziguli", 123);
//     this.year = year;
//     this.chargeTime = chargeTime;
//   }
// }

// const dziguli = new Dz("1997", "100000 years");

// class FindFigure {
//   constructor(selector = "#block") {
//     this.$el = document.querySelector(selector);
//   }
//   check() {
//     console.log("Work");
//   }
// }

// const elem = new FindFigure("#block");

// class FigureParams extends FindFigure {
//   constructor(width, height, color) {
//     super();
//     this.$el.style.width = width + "px";
//     this.$el.style.height = height + "px";
//     this.$el.style.backgroundColor = color;
//   }
// }

// const element = new FigureParams(100, 100, "red");
// element.check();

// let prototypeObj = {
//   property: 1,
//   setProperty: function (value) {
//     this.property += value;
//   },
// };

// let obj = {};
// obj.__proto__ = prototypeObj;

// obj.setProperty(2);

// console.log(prototypeObj);
// console.log(obj);

// let arr = [];
// console.log(arr);

// class Animal {
//   static sayHi() {
//     console.log("Hello World!");
//   }
// }

// class Rabbit extends Animal {}

// Rabbit.sayHi();

// const getRandomChance = () => Math.random() < 0.5;

// class Animal {
//   constructor(name) {
//     if (name.length < 3) {
//       this.name = "Unknow";
//     } else {
//       this.name = name;
//     }
//     this.animalWrapper = document.createElement("div");
//     this.animalName = document.createElement("span");
//     this.moveBtn = document.createElement("button");
//     this.location = 0;
//   }
//   render(selector) {
//     const container = document.querySelector(selector);
//     this.animalWrapper.className = "animal";
//     this.animalName.innerText = `${this.name}`;
//     this.moveBtn.innerText = "Move";
//     container.append(this.animalName);
//     container.append(this.animalWrapper);
//     container.append(this.moveBtn);
//     this.moveBtn.addEventListener("click", this.move.bind(this));
//   }
//   move() {
//     this.location += 20;
//     this.animalWrapper.style.transform = `translateX(${this.location}px)`;
//   }
//   say(selector) {
//     this.animalWrapper.classList.add(selector);
//     setTimeout(() => {
//       this.animalWrapper.classList.remove(selector);
//     }, 200);
//   }
// }

// class Dog extends Animal {
//   render(selector) {
//     super.render(selector);
//     this.animalWrapper.classList.add("dog");
//   }
//   move() {
//     super.move();
//     if (getRandomChance()) {
//       super.move();
//       this.say("dog-say");
//     }
//   }
// }

// class Cat extends Animal {
//   render(selector) {
//     super.render(selector);
//     this.animalWrapper.classList.add("cat");
//   }
//   move() {
//     if (getRandomChance()) {
//       super.move();
//     } else {
//       this.say("cat-say");
//     }
//   }
// }

// class Snake extends Animal {
//   constructor(name, type) {
//     super(name);
//     if (type !== "poison" && type !== "safe") {
//       this.type = "safe";
//     } else {
//       this.type = type;
//     }
//   }
//   render(selector) {
//     super.render(selector);
//     this.animalWrapper.classList.add("snake");
//   }
//   move() {
//     this.location += 5;
//     if (!getRandomChance()) {
//       super.move();
//       alert("Shhhhhhhhhhhhhhhhhhhhhhh");
//       if (this.type === "poison") {
//         document.querySelectorAll(".cat").forEach((e) => {
//           e.classList.remove("cat");
//         });
//         document.querySelectorAll(".dog").forEach((e) => {
//           e.classList.remove("dog");
//         });
//       }
//     } else {
//       super.move();
//     }
//   }
// }

// const dog = new Dog("Bobby");
// dog.render(".container");
// const cat = new Cat("Myrka");
// cat.render(".container");
// const snake = new Snake("Xenisa", "safe");
// snake.render(".container");

// const reg = class User {
//   constructor(role, login, email, password) {
//     (this._role = role), (this._login = login), (this.email = email);
//     this.password = password;
//     this.availableRoles = [
//       "super admin",
//       "admin",
//       "main manager",
//       "content manager",
//     ];
//   }
//   get role() {
//     return this._role;
//   }
//   set role(newRole) {
//     let isTrueRole;
//     this.availableRoles.forEach((element) => {
//       if (element !== newRole) {
//         return;
//       } else {
//         return (isTrueRole = element);
//       }
//     });
//     isTrueRole ? (this._role = isTrueRole) : console.log("wrong role");
//   }
//   get login() {
//     return this._login;
//   }
//   set login(newLogin) {
//     if (newLogin.length < 3) {
//       return console.log("Login must be at least 3 charackters");
//     } else {
//       this._login = newLogin;
//     }
//   }
//   isValidEmail(newEmail) {
//     if (
//       newEmail.includes("@") &&
//       newEmail.includes(".") &&
//     ) {
//       return (this.email = newEmail);
//     } else {
//       return console.log("Wrong e-mail");
//     }
//   }
//   getPasswordStrength() {
//     if (reg) {
//       return console.log("True");
//     }
//     return console.log(false);
//   }
// };

// const manager = new User("Manager", "sahs", "Al@gmail.com", "123");\

// function makeWorker() {
//   let name = "Pete";

//   return function () {
//     alert(name);
//   };
// }

// let name = "John";

// // create a function
// let work = makeWorker();

// // call it
// work();

// let value = "Сюрприз!";

// function f() {
//   let value = "ближайшее значение";

//   function g() {
//     debugger; // в консоли: напишите alert(value); Сюрприз!
//   }

//   return g;
// }

// let g = f();
// g();

// try {
//   alert("try");
//   if (confirm("Сгенерировать ошибку?")) BAD_CODE();
// } catch (e) {
//   alert("catch");
// } finally {
//   alert("finally");
// }

// function mult(n) {
//   return function multby() {
//     console.log(1000 * n);
//   };
// }
// const test1 = mult();
// const test2 = mult(10);

// console.log(test1(200));
// console.log(test2());

// function createIncrementor(n) {
//   return function (num) {
//     return n * num;
//   };
// }

// const test1 = createIncrementor(2);
// console.log(test1(20));

// function logPerson() {
//   console.log(`Person: ${this.name}, ${this.age}, ${this.job}`);
// }

// const person1 = { name: "Michael", age: 25, job: "FrontEnd" };

// const person2 = { name: "Helen", age: 35, job: "BackEnd" };

// function bind(obj, func) {
//   return function (...args) {
//     func.call(obj, ...args);
//   };
// }

// bind(person1, logPerson)();
// bind(person2, logPerson);

// function counter() {
//   let count = 0;
//   return function () {
//     count = count + 1;
//     return count;
//   };
// }

// let startCounter = counter();

// console.log(startCounter());
// console.log(startCounter());
// console.log(startCounter());
// console.log(startCounter());
// console.log(startCounter());
// console.log(startCounter());
// console.log(startCounter());
// console.log(startCounter());

// function foo() {
//   try {
//     return 1;
//   } catch (e) {
//     console.log(2);
//   } finally {
//     console.log(3);
//   }
// }

// console.log(foo());
// const student = {
//   name: "John Doe",
//   age: 16,
//   scores: {
//     maths: 74,
//     english: 63,
//   },
// };

// const {
//   name: title,
//   age: old,
//   scores: { maths: mat, english: eng },
// } = student;

// console.log(title, eng);

// const rainbow = [
//   "red",
//   "orange",
//   "yellow",
//   "green",
//   "blue",
//   "indigo",
//   "violet",
// ];

// const [, , yel, ...other] = rainbow;

// console.log(yel);

// console.log(other);

// const student = {
//   name: "John Doe",
//   age: 16,
//   scores: {
//     maths: 74,
//     english: 63,
//     science: 85,
//   },
// };

// function displayScore({
//   name,
//   age,
//   scores: { maths = 0, english = 0, science = 0 } = {},
// } = {}) {
//   console.log(
//     `Studnet ${name} recieve ${maths} with math, ${english} with eng`
//   );
// }

// displayScore();

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

// const container = document.querySelector("#root");

// function drawAuthors(container) {
//   nextStep: for (let i = 0; i < books.length; i++) {
//     const newUL = document.createElement("ul");
//     const { name, author, price } = books[i];
//     if ((name && author && price) === undefined) {
//       console.log(`Not enough data, mistake at ${mistake}`);
//       continue nextStep;
//     } else {
//       newUL.insertAdjacentHTML(
//         "afterbegin",
//         `<li>${name}</li>
//         <li>${author}</li>
//         <li>${price}</li>`
//       );
//       container.append(newUL);
//     }
//   }
// }

// function ReadEror(message, cause) {
//   this.message = message;
//   this.cause = cause;
//   this.name = "ReadError";
//   this.stack = cause.stack;
// }

// homework 2 start

// const container = document.querySelector("#root");
// function drawAuthors(container, obj) {
//   for (let i = 0; i < obj.length; i++) {
//     try {
//       const { name, author, price } = obj[i];
//       if (!name) {
//         throw new SyntaxError(`name is not indicated at obj num ${[i + 1]}`);
//       } else if (!author) {
//         throw new SyntaxError(`author is not indicated at obj num ${[i + 1]}`);
//       } else if (!price) {
//         throw new SyntaxError(`price is not indicated at obj num ${[i + 1]}`);
//       }
//       const newUL = document.createElement("ul");
//       newUL.insertAdjacentHTML(
//         "afterbegin",
//         `<li>${name}</li>
//         <li>${author}</li>
//         <li>${price}</li>`
//       );
//       container.append(newUL);
//     } catch (error) {
//       if (error.name === "SyntaxError") {
//         console.log("JSON Error:" + error.message);
//       }
//     }
//   }
// }

// drawAuthors(container, books);

// homework 2 end

// homework 3 start

// const clients1 = [
//   "Гилберт",
//   "Сальваторе",
//   "Пирс",
//   "Соммерс",
//   "Форбс",
//   "Донован",
//   "Беннет",
// ];
// const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

// function startArray(array, pushTo) {
//   for (let i = 0; i < array.length; i++) {
//     if (typeof array[i] === "object") {
//       startArray(array[i], pushTo);
//     } else {
//       pushTo.push(array[i]);
//     }
//   }
// }
// const makeUniq = (...args) => {
//   let newArr = [];
//   let argsLength = args.length;
//   if (argsLength > 1) {
//     for (let i = 0; i < argsLength; i++) {
//       startArray(args[i], newArr);
//     }
//   } else {
//     newArr.push(...args);
//   }
//   const uniqObj = new Set(newArr);
//   return [...uniqObj];
// };

// const margedClients = makeUniq(clients1, clients2);
// console.log(margedClients);

// homework3 end

const posts = [
  {
    id: 1,
    title:
      "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
    body: "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto",
  },
  {
    id: 2,
    title: "qui est esse",
    body: "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla",
  },
  {
    id: 3,
    title: "ea molestias quasi exercitationem repellat qui ipsa sit aut",
    body: "et iusto sed quo iure\nvoluptatem occaecati omnis eligendi aut ad\nvoluptatem doloribus vel accusantium quis pariatur\nmolestiae porro eius odio et labore et velit aut",
  },
  {
    id: 4,
    title: "eum et est occaecati",
    body: "ullam et saepe reiciendis voluptatem adipisci\nsit amet autem assumenda provident rerum culpa\nquis hic commodi nesciunt rem tenetur doloremque ipsam iure\nquis sunt voluptatem rerum illo velit",
  },
  {
    id: 5,
    title: "nesciunt quas odio",
    body: "repudiandae veniam quaerat sunt sed\nalias aut fugiat sit autem sed est\nvoluptatem omnis possimus esse voluptatibus quis\nest aut tenetur dolor neque",
  },
  {
    id: 6,
    title: "dolorem eum magni eos aperiam quia",
    body: "ut aspernatur corporis harum nihil quis provident sequi\nmollitia nobis aliquid molestiae\nperspiciatis et ea nemo ab reprehenderit accusantium quas\nvoluptate dolores velit et doloremque molestiae",
  },
  {
    id: 7,
    title: "magnam facilis autem",
    body: "dolore placeat quibusdam ea quo vitae\nmagni quis enim qui quis quo nemo aut saepe\nquidem repellat excepturi ut quia\nsunt ut sequi eos ea sed quas",
  },
  {
    id: 8,
    title: "dolorem dolore est ipsam",
    body: "dignissimos aperiam dolorem qui eum\nfacilis quibusdam animi sint suscipit qui sint possimus cum\nquaerat magni maiores excepturi\nipsam ut commodi dolor voluptatum modi aut vitae",
  },
  {
    id: 9,
    title: "nesciunt iure omnis dolorem tempora et accusantium",
    body: "consectetur animi nesciunt iure dolore\nenim quia ad\nveniam autem ut quam aut nobis\net est aut quod aut provident voluptas autem voluptas",
  },
  {
    id: 10,
    title: "optio molestias id quia eum",
    body: "quo et expedita modi cum officia vel magni\ndoloribus qui repudiandae\nvero nisi sit\nquos veniam quod sed accusamus veritatis error",
  },
];

// class Card {
//   constructor(editAction, deleteAction) {
//     this.divCard = document.createElement("div");
//     this.bttnEdit = document.createElement("button");
//     this.bttnDel = document.createElement("button");
//     this.editAction = editAction;
//     this.deleteAction = deleteAction;
//     this.contentContainer = document.createElement("div");
//   }

//   createElem() {
//     this.divCard.append(this.bttnEdit);
//     this.divCard.append(this.bttnDel);
//     // this.divCard.insertAdjacentHTML('beforeend', '<div class="card__content-container"></div>')
//     this.contentContainer.classList.add("card__content-container");
//     this.divCard.append(this.contentContainer);
//     this.divCard.className = "card";
//     this.bttnEdit.className = "card__btn card__edit";
//     this.bttnDel.className = "card__btn card__delete";
//   }

//   render(container = document.body) {
//     this.createElem();
//     container.append(this.divCard);

//     this.bttnEdit.addEventListener("click", this.editAction.bind(this));
//     this.bttnDel.addEventListener("click", this.deleteAction.bind(this));
//   }
// }

// const testCard = new Card(edit, deleteF);

// testCard.render();

// class ArticleCard extends Card {
//   constructor(title, text, editAction, deleteAction) {
//     super(editAction, deleteAction);
//     this.title = title;
//     this.text = text;
//     this.titleContainer = document.createElement("h3");
//     this.textContainer = document.createElement("p");
//   }
//   createElem() {
//     super.createElem();
//     this.titleContainer.innerText = this.title;
//     this.textContainer.innerText = this.text;
//     this.contentContainer.append(this.titleContainer, this.textContainer);
//     // this.contentContainer.append(this.textContainer);
//   }
// }

// class Modal {
//   constructor() {
//     this._modalElement = document.createElement("div");
//     this._backgroundContainer = document.createElement("div");
//     this._mainContainer = document.createElement("div");
//     this._contentContainer = document.createElement("div");
//     this._buttonContainer = document.createElement("div");
//     this._closeButton = document.createElement("div");
//   }

//   closeModal() {
//     this._modalElement.remove();
//   }

//   createElements() {
//     this._closeButton.classList.add("modal__close");
//     this._closeButton.addEventListener("click", this.closeModal.bind(this));

//     this._backgroundContainer.classList.add("modal__background");
//     this._backgroundContainer.addEventListener(
//       "click",
//       this.closeModal.bind(this)
//     );

//     this._contentContainer.classList.add("modal__content-wrapper");
//     this._buttonContainer.classList.add("modal__button-wrapper");

//     this._mainContainer.classList.add("modal__main-container");
//     this._mainContainer.append(this._contentContainer);
//     this._mainContainer.append(this._buttonContainer);
//     this._mainContainer.append(this._closeButton);

//     this._modalElement.classList.add("modal");
//     this._modalElement.append(this._backgroundContainer);
//     this._modalElement.append(this._mainContainer);
//   }

//   render(container = document.body) {
//     this.createElements();
//     container.append(this._modalElement);
//   }
// }

// class DeleteModal extends Modal {
//   constructor(title, submitButtonHandler) {
//     super();

//     this._title = title;
//     this._submitButtonHandler = submitButtonHandler;
//     this._submitButton = document.createElement("button");
//     this._cancelButton = document.createElement("button");
//   }

//   createElements() {
//     super.createElements();

//     this._submitButton.classList.add("modal__confirm-btn");
//     this._cancelButton.classList.add("modal__cancel-btn");

//     this._submitButton.innerText = "Confirm";
//     this._cancelButton.innerText = "Cancel";

//     this._contentContainer.insertAdjacentHTML(
//       "beforeend",
//       `<h3>Do you really want to delete "${this._title}"?</h3>`
//     );
//     this._buttonContainer.append(this._submitButton, this._cancelButton);

//     this._submitButton.addEventListener("click", () => {
//       this._submitButtonHandler();
//       this.closeModal();
//     });
//     this._cancelButton.addEventListener("click", this.closeModal.bind(this));
//   }
// }

// // new DeleteModal(posts[0].title, submitButtonHandler).render();

// posts.forEach((el) => {
//   new ArticleCard(el.title, el.body, edit, deleteF).render();
// });

// function edit() {
//   console.log("edit", this.title);
// }

// function deleteF() {
//   const submitButtonHandler = () => {
//     this.divCard.remove();
//   };

//   new DeleteModal(this.title, submitButtonHandler).render();
// }

// class EditModal extends Modal {
//   createElements() {
//     super.createElements();
//   }
// }

// {
//   /* <form>
// <label>Title</label>
// <input>
// <label>Post</label>
// <textarea></textarea>
// </form> */
// }

// class Card {
//   constructor(handleDeleteBtn, handleEditBtn) {
//     this.divCard = document.createElement("div");
//     this._handleDeleteBtn = handleDeleteBtn;
//     this._handleEditBtn = handleEditBtn;
//     this.buttonEdit = document.createElement("button");
//     this.buttonDelete = document.createElement("button");
//     this.contentContainer = document.createElement("div");
//   }
//   createElement() {
//     this.divCard.classList.add("card");
//     this.buttonEdit.classList.add("card__btn", "card__edit");
//     this.buttonDelete.classList.add("card__btn", "card__delete");
//     this.contentContainer.classList.add("card__content-container");
//     this.divCard.append(
//       this.buttonEdit,
//       this.buttonDelete,
//       this.contentContainer
//     );
//   }
//   render(container = document.body) {
//     this.createElement();
//     container.append(this.divCard);
//     this.buttonEdit.addEventListener("click", () => {
//       this._handleEditBtn();
//     });
//     this.buttonDelete.addEventListener("click", () => {
//       this._handleDeleteBtn();
//     });
//   }
// }

// function handleEdit() {
//   const confirmF = (newTitle, newText) => {
//     this.contentTitle.innerText = newTitle;
//     this.contentParagraph.innerText = newText;
//   };
//   new EditModal(this.title, this.paragraph, confirmF).render();
// }
// function handleDelete() {
//   const submitButtonHandler = () => {
//     this.divCard.remove();
//   };
//   new DeleteModal(this.title, submitButtonHandler).render();
// }

// class ArticleCard extends Card {
//   constructor(title, paragraph, handleDeleteBtn, handleEditBtn) {
//     super(handleDeleteBtn, handleEditBtn);
//     this.contentTitle = document.createElement("h3");
//     this.contentParagraph = document.createElement("p");
//     this.title = title;
//     this.paragraph = paragraph;
//   }
//   createElement() {
//     super.createElement();
//     this.contentTitle.innerText = this.title;
//     this.contentParagraph.innerText = this.paragraph;
//     this.contentContainer.append(this.contentTitle, this.contentParagraph);
//   }
// }

// posts.forEach((el) => {
//   new ArticleCard(el.title, el.body, handleDelete, handleEdit).render();
// });
// class Modal {
//   constructor() {
//     this.modalContainer = document.createElement("div");
//     this.backgroundContainer = document.createElement("div");
//     this.modalTextContainer = document.createElement("div");
//     this.modalCloseBtn = document.createElement("button");
//     this.modalContentWrapper = document.createElement("div");
//     this.modalButtonWrapper = document.createElement("div");
//   }
//   closeModal() {
//     this.modalContainer.remove();
//   }
//   createElement() {
//     this.modalContainer.classList.add("modal");
//     this.backgroundContainer.classList.add("modal__background");
//     this.modalTextContainer.classList.add("modal__main-container");
//     this.modalCloseBtn.classList.add("modal__close");
//     this.modalContentWrapper.classList.add("modal__content-wrapper");
//     this.modalButtonWrapper.classList.add("modal__button-wrapper");
//     this.modalTextContainer.append(
//       this.modalCloseBtn,
//       this.modalContentWrapper,
//       this.modalButtonWrapper
//     );
//     this.modalCloseBtn.addEventListener("click", this.closeModal.bind(this));
//     this.backgroundContainer.addEventListener(
//       "click",
//       this.closeModal.bind(this)
//     );
//   }
//   render(container = document.body) {
//     this.createElement();
//     this.modalContainer.append(
//       this.backgroundContainer,
//       this.modalTextContainer
//     );
//     container.append(this.modalContainer);
//   }
// }

// class DeleteModal extends Modal {
//   constructor(modalTitle, submitButtonHandler) {
//     super();
//     this.modalTitle = modalTitle;
//     this.modalCancelBtn = document.createElement("button");
//     this.modalConfirmBtn = document.createElement("button");
//     this._submitButtonHandler = submitButtonHandler;
//   }
//   createElement() {
//     super.createElement();
//     this.modalConfirmBtn.classList.add("modal__confirm-btn");
//     this.modalCancelBtn.classList.add("modal__cancel-btn");
//     this.modalContentWrapper.insertAdjacentHTML(
//       "beforebegin",
//       `<h3>Do you really want to delete "${this.modalTitle}"?</h3>`
//     );
//     this.modalConfirmBtn.innerText = "Confirm";
//     this.modalCancelBtn.innerText = "Cancel";
//     this.modalButtonWrapper.append(this.modalConfirmBtn, this.modalCancelBtn);
//     this.modalConfirmBtn.addEventListener("click", () => {
//       this.closeModal();
//       this._submitButtonHandler();
//     });
//     this.modalCancelBtn.addEventListener("click", () => {
//       this.closeModal();
//     });
//   }
// }

// class EditModal extends Modal {
//   constructor(editTitle, editText, confirmFunc) {
//     super();
//     this.form = document.createElement("form");
//     this.labelTitle = document.createElement("label");
//     this.input = document.createElement("input");
//     this.labelPost = document.createElement("label");
//     this.textArea = document.createElement("textarea");
//     this.editText = editText;
//     this.editTitle = editTitle;
//     this.confirmEditBtn = document.createElement("button");
//     this.confirmFunc = confirmFunc;
//   }
//   createElement() {
//     super.createElement();
//     this.labelTitle.innerText = "Title";
//     this.labelPost.innerText = "Post";
//     this.confirmEditBtn.classList.add("modal__confirm-btn");
//     this.confirmEditBtn.innerText = "Confirm";
//     this.input.value = this.editTitle;
//     this.textArea.value = this.editText;
//     this.form.append(
//       this.labelTitle,
//       this.input,
//       this.labelPost,
//       this.textArea
//     );
//     this.modalContentWrapper.append(this.form);
//     this.modalButtonWrapper.append(this.confirmEditBtn);
//     this.confirmEditBtn.addEventListener("click", () => {
//       this.confirmFunc(this.input.value, this.textArea.value);
//       this.closeModal();
//     });
//   }
// }

// game start

// function getRandomInt(max) {
//   return Math.floor(Math.random() * max);
// }

// function colorActive() {
//   let randomNum = getRandomInt(15);
//   let item = document.querySelectorAll(".table__row-field")[randomNum];
//   item.classList.add("activeCLickMe");
// }

// function colorDisabled() {
//   console.log(123);
//   console.log(this);
// }

// function startOrPauseGame() {
//   let timerActive = setTimeout(function tick() {
//     colorActive();
//     timerActive = setTimeout(tick, 2000);
//   }, 200);
//   let timerOver = setTimeout(
//     function end() {
//       this.colorPc();
//       timerOver = setTimeout(end.bind(this), 2000);
//     }.bind(this),
//     2000
//   );
// }

// class tableField {
//   constructor() {
//     this.table = document.createElement("table");
//     this.tableRow = document.createElement("tr");
//     this.tableRowField = document.createElement("td");
//     this.gameWrapper = document.createElement("div");
//   }

//   colorUser(e) {
//     e.target.style.backgroundColor = "green";
//   }
//   colorPc(e) {
//     e.style.backgroundColor = "red";
//   }
//   createElement() {
//     this.table.classList.add("table");
//     this.gameWrapper.classList.add("game-wrapper");
//     this.tableRow.classList.add("table__row");
//     this.tableRowField.classList.add("table__row-field");
//     this.tableRow.append(
//       this.tableRowField,
//       this.tableRowField.cloneNode(true),
//       this.tableRowField.cloneNode(true),
//       this.tableRowField.cloneNode(true)
//     );
//     this.table.append(
//       this.tableRow,
//       this.tableRow.cloneNode(true),
//       this.tableRow.cloneNode(true),
//       this.tableRow.cloneNode(true)
//     );
//     this.gameWrapper.append(this.table);
//   }
//   render(container = document.querySelector(".container")) {
//     this.createElement();
//     container.append(this.gameWrapper);
//   }
// }

// class startGame extends tableField {
//   constructor(timingFunc) {
//     super();
//     this.buttonStart = document.createElement("div");
//     this.timingFunc = timingFunc;
//   }
//   createElement() {
//     super.createElement();
//     this.buttonStart.classList.add("start-btn");
//     this.gameWrapper.append(this.buttonStart);
//   }
//   render() {
//     super.render();
//     this.buttonStart.addEventListener("click", () => {
//       this.timingFunc();
//     });
//     this.table.addEventListener("click", () => {
//       this.timingFunc();
//     });
//   }
// }

// class UserPlayer extends startGame {
//   render() {
//     super.render();
//     this.table.addEventListener("click", (e) => {
//       if (e.target !== this.table) {
//         this.colorUser(e);
//       }
//     });
//   }
// }

// class PCPlayer extends startGame {
//   render() {
//     super.render();
//     this.table.addEventListener("click", (e) => {
//       if (e.target !== this.table) {
//         this.colorPc(e);
//       }
//     });
//   }
// }

// new UserPlayer(startOrPauseGame).render();

// game end

// const colors = [
//   "#92a8d1",
//   "#deeaee",
//   "#b1cbbb",
//   "#c94c4c",
//   "#f7cac9",
//   "#f7786b",
//   "#82gdfgb74b",
//   "#405d27",
//   "#405d271231",
//   "#b5e7a0",
//   "#eca1a6",
//   "#d64161",
//   "#b2ad7f",
//   "#6b5b95",
// ];

// class HexItem {
//   constructor(color) {
//     if (color.length > 7) {
//       throw new NotHexError(color);
//     }
//     this.color = color;
//     this.divItem = document.createElement("div");
//     this.span = document.createElement("span");
//   }
//   createElement() {
//     this.divItem.classList.add("item");
//     this.divItem.style.backgroundColor = `${this.color}`;
//     this.span.innerText = this.color;
//     this.divItem.append(this.span);
//   }
//   render(container = document.querySelector(".colors-container")) {
//     this.createElement();
//     container.append(this.divItem);
//   }
// }

// class NotHexError extends Error {
//   constructor(color) {
//     super();
//     this.name = NotHexError;
//     this.message = `Error with HEX at ${color}`;
//   }
// }

// colors.forEach((el) => {
//   try {
//     new HexItem(el).render();
//   } catch (err) {
//     if (err instanceof NotHexError) {
//       console.warn(err);
//     } else {
//       throw err;
//     }
//   }
// });

// # Палитра цветов
// <hr>

// Дан массив цветов в формате HEX

// Выведите на экран палитру состоящую из:

// ```html
// <div class="item">
//     <span>#000000</span>
// </div>
// ```

// где `#000000` это код цвета.

// К `<div class="item">` добавьте свойство `background-color` с соответствующим цветом.

// Если код цвета не соответствует формату HEX - выведет в консоль ошибку и не отрисовывайте блок в палитру;

// P.S. Используйте `try...catch`, свой тип ошибки наследуемый от `Error`, все остальные ошибки пробросьте дальше.

// const array = [1, 3, 2, 4, 4, 5, 6, 11, 2, 31];

// const inBetween = (a, b) => {};
// const inArray = (array) => {};

// const inBetween3And5 = (a, b) => {
//   return (el) => el >= 5 && el <= b;
// };

// const inBetween3And5Func = inBetween3And5(4, 5);
// console.log(array.filter(inBetween3And5Func));
