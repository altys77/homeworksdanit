"use strict";

// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

// 1) Коли ми працюємо з 3rd party data
// 2) Коли працюємо з данними які є динамічними та прилітають до нас з BackEnd

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

class BooksData {
  constructor(author, name, price, indx) {
    if (!name) {
      throw new DataError("Name", indx);
    } else if (!author) {
      throw new DataError("author", indx);
    } else if (!price) {
      throw new DataError("price", indx);
    }
    this.ul = document.createElement("ul");
    this.name = name;
    this.author = author;
    this.price = price;
  }
  createElement() {
    this.ul.insertAdjacentHTML(
      "afterbegin",
      `<li>${this.author}</li>
    <li>${this.name}</li>
    <li>${this.price}</li>`
    );
  }
  render(container = document.querySelector("#root")) {
    this.createElement();
    container.append(this.ul);
  }
}

class DataError extends Error {
  constructor(el, indx) {
    super();
    this.name = DataError;
    this.message = `The ${el} is missed at ${indx + 1} object`;
  }
}

books.forEach((el, indx) => {
  try {
    new BooksData(el.author, el.name, el.price, indx).render();
  } catch (err) {
    if (err instanceof DataError) {
      console.warn(err.message);
    }
  }
});
