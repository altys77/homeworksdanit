"use strict"

const headerDate = document.querySelector("#date");

const monthArr = [
    "Jan",
    "Feb",
    "March",
    "Aprl",
    "May",
    "June",
    "July",
    "Aughst",
    "Septm",
    "Oct",
    "Nov",
    "Dec",
]

const setDate = (element) => {
    const currentDate = new Date()
    const getLetteredMonth = monthArr[currentDate.getMonth()];
    const formatedDate = `${currentDate.getDate()} ${getLetteredMonth}, ${currentDate.getFullYear()}`
    element.classList.add("date-style");
   return element.innerText = formatedDate;
}

setDate(headerDate);

const datasetAltCollection = document.querySelectorAll(".dog-gallery img");




const removeAltAndMissedSrcItems = (array) => {
    array.forEach(element => {
        if (!element.src) {
            return element.remove();
        }
        element.alt = element.dataset.alt ? element.dataset.alt : "";
    })
}

removeAltAndMissedSrcItems(datasetAltCollection);


const findReservedDogs = (reservedDogs) => {
    reservedDogs.forEach(element => {
        if (element.dataset.reseved) {
            const divWrapper = document.createElement("div");
            divWrapper.classList.add("reserved-wrapper");
            divWrapper.innerHTML = `
            <div class="reserved-background"></div>
            <span class="reserved-text">RESERVED</span>      
            `;
            divWrapper.append(element.cloneNode(true));
            element.replaceWith(divWrapper);
        }
    })
}

findReservedDogs(datasetAltCollection)
