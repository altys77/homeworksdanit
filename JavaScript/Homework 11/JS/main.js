"use strict"

const getInputs = document.querySelectorAll(".input-wrapper > input");
const getIcons = document.querySelectorAll(".icon-password")
const getListener = document.querySelector(".password-form");
const submitBtn = document.querySelector(".btn");
const wrongPasswordAlert = document.querySelector(".span-red");

const changeIcons = (array) => {
    let flag = false;
    array.forEach(element => {
        if (element.className === "fas fa-eye icon-password") {
            flag = true;
            return element.className = "fas fa-eye-slash icon-password";
        }
        element.className = "fas fa-eye icon-password";
    });
    return flag;
}

const showPassword = (inputArray, flag) => {
    inputArray.forEach(element => {
        if (flag) {
            return element.type = "text";
        }
        element.type = "password";
    });
}

const checkPassword = () => {
    const firstInput = document.querySelector(".first-password");
    const secondInput = document.querySelector(".second-password");
    if (firstInput.value === secondInput.value && (firstInput.value && secondInput.value)) {
        wrongPasswordAlert.classList.add("display-none");
        firstInput.value = "";
        secondInput.value = "";
        return alert("You are welcome");
    }
    wrongPasswordAlert.classList.toggle("display-none", false);
}


getListener.addEventListener("click", (event) => {
    if(event.target.closest("i")) {
        const flag = changeIcons(getIcons);
        showPassword(getInputs, flag);
    }
})


submitBtn.addEventListener("click", (event) => {
    event.preventDefault();
    let checkPass = checkPassword();
})

getListener.addEventListener("change", (event) => {
    if (event.target.closest("input")) {
        wrongPasswordAlert.classList.toggle("display-none", true);
    }
})



