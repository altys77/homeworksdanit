"use strict"


// Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.

// setTimeout - делает одно действие через какое-то время (если не использовать рекурсию)
// setInterval - посторяет одно действие в течении заданного времени

// Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?

// Не сработает, нулевая задержка невозможно по причине того что setTimeout кинет в стек вызовов где он будет ожидать своей очереди на выплнение

// Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?

// Для того чтобы цикл не был вечным и была возможность его остановить

const imgCollection = document.querySelectorAll(".image-to-show");
const startBtn = document.querySelector(".startBtn");
const endBtn = document.querySelector(".endBtn")
const control = document.querySelector(".controlInstruments");
let currnetIndex = 0;
let showImages;


const switchImg = (collection) => {
    collection[currnetIndex].classList.toggle("display-none", true);
    currnetIndex++;
    if ((currnetIndex +1 ) > collection.length) {
        currnetIndex = 0;
    }
    collection[currnetIndex].classList.toggle("display-none", false);
}

control.addEventListener("click", (event) => {
    if(event.target.closest(".startBtn")) {
        event.target.toggleAttribute("disabled", true);
        showImages = setTimeout(function startToShow () {
            switchImg(imgCollection);
            showImages = setTimeout(startToShow, 3000);
        }, 3000);
        endBtn.toggleAttribute("disabled", false);
    }
    if(event.target.closest(".endBtn")) {
        clearTimeout(showImages);
        endBtn.toggleAttribute("disabled", true);
        startBtn.innerText = "Resume showing";
        startBtn.toggleAttribute("disabled", false);
    }
})

document.addEventListener("DOMContentLoaded", () => {
    imgCollection[0].classList.remove("display-none");
})



// WORK

// startBtn.addEventListener("click", (event) => {
//     event.target.toggleAttribute("disabled", true);
//     endBtn.classList.toggle("display-none", false);
//     endBtn.toggleAttribute("disabled", false);
//     let startToShow = setTimeout(function next() {
//         switchImg(imgCollection);
//         startToShow = setTimeout(next, 3000);
//     }, 3000);
//     endBtn.addEventListener("click", () => {
//         clearTimeout(startToShow);
//         endBtn.toggleAttribute("disabled", true);
//         startBtn.toggleAttribute("disabled", false)
//     })
// })

// document.addEventListener("DOMContentLoaded", () => {
//     imgCollection[0].classList.remove("display-none");
// })