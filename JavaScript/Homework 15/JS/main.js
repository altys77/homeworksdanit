"use strict"

// 1) Напишіть, як ви розумієте рекурсію. Навіщо вона використовується на практиці?
// Рекурсія подтрібна для повтороного виклику функції в коді доки якась умова не буде виконана (а також коли функція викликає саму себе в коді доки зазначена умова не виконається). 


const findFactorial = (userNumber) => {
    if (+userNumber === 1) {
        return 1;
    };
    return userNumber * findFactorial(userNumber - 1);
}

const askNumber = () => {
    let userNumber = prompt("Type your number here");
    while (!Number.isInteger(+userNumber) || isNaN(+userNumber) || +userNumber === 0) {
        userNumber = prompt("Type integner number please instead of ",`${userNumber}`);
    }
    let result = findFactorial(userNumber);
    return result
}


const findFactorialNumber = askNumber();
console.log(findFactorialNumber);





