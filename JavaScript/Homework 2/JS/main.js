"use strict"

// Теоретичні питання
// Які існують типи даних у Javascript?
// У чому різниця між == і ===?
// Що таке оператор?

// 1) String, Number, Boolean, null, undefined, BigInt, Object.
// 2) Суворе та не суворе рівенство. Суворе - перевіряємо тип данних та число/строку т.д., якщо тип данних різній (напраклад "6" === 6) то будє false. Не суворий - зведення до 1 типу данних та перевірка.
// 3) Оператори які допомогають нам зробити якусь дію (наприклад, +, -, *)

let userName = prompt("Type your Name");
while (!userName) {
    userName = prompt(`Please, type userName`,`${userName}`);
}

let userAge = prompt("Type your age here");
while (!Number.isInteger(+userAge) || !(+userAge)) {
    userAge = prompt(`Type your age here please`,`${userAge}`);
}
userAge = Number(userAge);

if (userAge < 18) {
    alert("You are not allowed to visit this website")
} else if (userAge >= 18 && userAge <= 22) {
    const checkAccess = confirm("Are you sure you want to continue?")
    if (checkAccess) {
        alert(`Welcome, ${userName}`);
    } else {
        alert("You are not allowed to visit this website")
    }
} else {
    alert(`Welcome, ${userName}`);
}

