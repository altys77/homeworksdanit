"use strict"


// 1) Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування

// Екранування, це коли ми маємо вставити якись символ у строку який вже має виконувати якусь дію у Javascript. Наприклад, ми маємо строку (`I\`m 125 years old.`) Для того щоб Javascript зміг обробити цей запит ми миємо перед символом ' поставити \ що буде означати наступний символ покажи як символ а не як якусь дію JS.

// 2) Які засоби оголошення функцій ви знаєте?

// Funtion declaration - function doSomething() {alert("Hi")};
// Function expression - const doSomething = () => {alert("Hi")};
// Named function Expression - const doSomething = function () {alert("Hi")} (для роботи з рекурсією)

// 3) Що таке hoisting, як він працює для змінних та функцій?
// Хостінг це коли браузер починає считувати нашу сторінкую Спочатку він зчитає функції та запише їх в пам'ять а потім почне зчитувати весь код який є у нас на сторінці

const askUserData = (message) => {
    let data = prompt(message);
    while (!data) {data = prompt(message)};
    return data;
}


const createNewUser = () => {
    return {
        userName: askUserData("Enter name"),
        userSurname: askUserData("Enter Surname"),
        birthday: new Date (askUserData("Enter your date of birth dd.mm.yyyy").split(".").reverse().join(".")),
        getAge () {
            const birthdayDate = new Date(this.birthday);
            const currentDate = new Date();
            const birthdayCheckToCurrentYear = new Date(birthdayDate).setFullYear(currentDate.getFullYear());
            const age = currentDate.getFullYear() - birthdayDate.getFullYear(); 
            if (+currentDate < birthdayCheckToCurrentYear) {
                return age -1;
            }
            return age;
        },
        getLogin () {
            return (this.userName[0] + this.userSurname).toLowerCase();
        },
        getPassword() {
            return (this.userName[0].toUpperCase() + this.userSurname.toLowerCase() + (this.birthday.getFullYear()));
        },
    }
}

const newUser = createNewUser();

console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());




















