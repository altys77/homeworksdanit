import React from "react";
import Card from "../Card/Card";
import styles from "./Main.module.scss";
import PropTypes from "prop-types";

class Main extends React.PureComponent {
  render() {
    const {
      goods,
      ratesClickHandler,
      handleAddToCartModal,
      handleFavoritesClick,
      inFavorites,
    } = this.props;
    const { goodsWrapper } = styles;
    return (
      <main>
        <h2> E-Shop</h2>
        <div className={goodsWrapper}>
          {goods ? (
            goods.map((el) => (
              <Card
                handleFavoritesClick={handleFavoritesClick}
                handleAddToCartModal={handleAddToCartModal}
                ratesClickHandler={ratesClickHandler}
                item={el}
                key={el.EAN}
                id={el.EAN}
                inFavorites={inFavorites}
              />
            ))
          ) : (
            <h2>Loading</h2>
          )}
        </div>
      </main>
    );
  }
}

export default Main;

Main.propTypes = {
  goods: PropTypes.array,
  ratesClickHandler: PropTypes.func,
  handleAddToCartModal: PropTypes.func,
  handleFavoritesClick: PropTypes.func,
  inFavorites: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
    PropTypes.number,
  ]),
};

Main.defaultProps = {
  goods: [],
  ratesClickHandler: () => {},
  handleAddToCartModal: () => {},
  handleFavoritesClick: () => {},
  inFavorites: false,
};
