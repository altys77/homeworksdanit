import React from "react";
import styles from "./Modal.module.scss";
import PropTypes, { array, oneOf, oneOfType } from "prop-types";

class Modal extends React.PureComponent {
  render() {
    const {
      modal__background,
      modal__component,
      modal__close,
      modal__text,
      modal__buttons,
    } = styles;
    const { header, text, needCrossBtn, actions, handleCloseModal } =
      this.props;
    return (
      <div className={modal__background} onClick={handleCloseModal}>
        <div className={modal__component} onClick={(e) => e.stopPropagation()}>
          <div>
            <h2>{header}</h2>
            {needCrossBtn && (
              <div className={modal__close} onClick={handleCloseModal}></div>
            )}
          </div>
          <p className={modal__text}>{text}</p>
          <div className={modal__buttons}>{actions}</div>
        </div>
      </div>
    );
  }
}

export default Modal;

Modal.propTypes = {
  header: PropTypes.string,
  text: PropTypes.string,
  needCrossBtn: PropTypes.bool,
  actions: PropTypes.node,
  handleCloseModal: PropTypes.func,
};

Modal.defaultProps = {
  header: "",
  text: "",
  needCrossBtn: false,
  actions: <></>,
  handleCloseModal: () => {},
};
