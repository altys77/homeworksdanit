import React, { useEffect, useState } from "react";
import Header from "./Components/Header/Header";
import Modal from "./Components/Modal/Modal.";
import Button from "./Components/Button/Button";
import AppRoutes from "./AppRoutes";

const App = () => {
  let [isLoaded, setIsLoaded] = useState(false);
  let [goods, setGoods] = useState([]);
  let [displayCart, setDisplayCart] = useState(false);
  let [goodsAtCartCounter, setGoodsAtCartCounter] = useState(0);
  let [openModal, setOpenModal] = useState(false);
  let [modalProps, setModalProps] = useState("");
  let [inFavorites, setInFavorites] = useState(0);

  const handleCloseModal = (e) => {
    if (
      e.target.closest("div.Modal_modal__close__P7gzr") ||
      e.target.closest("div.Modal_modal__background__m2l9K")
    ) {
      setOpenModal(!openModal);
    }
  };

  const handleFavoritesClick = (e, id) => {
    const copiedArr = goods;
    const findElIndex = copiedArr.findIndex(({ EAN }) => EAN === id);
    if (copiedArr[findElIndex].inFavorites) {
      copiedArr[findElIndex].inFavorites = false;

      const nextFavoriteCounter = parseInt(inFavorites) - 1;
      localStorage.setItem("inFavorites", nextFavoriteCounter);
      setInFavorites(nextFavoriteCounter);
    } else {
      copiedArr[findElIndex].inFavorites = true;

      const nextFavoriteCounter = parseInt(inFavorites) + 1;
      localStorage.setItem("inFavorites", nextFavoriteCounter);
      setInFavorites(nextFavoriteCounter);
    }
    setGoods([...copiedArr]);
    localStorage.setItem("goods", JSON.stringify(goods));
  };
  const showHideBasket = (e) => {
    setDisplayCart(!displayCart);
  };

  const deleteAllItemsFromCart = (e, id) => {
    let prevQty;
    const copiedGoodsArr = goods.map((el) => {
      if (el.EAN === id) {
        prevQty = el.qtyInOrder;
        el.qtyInOrder = 0;
        el.cart = "false";
      }
      return el;
    });
    setGoods([...copiedGoodsArr]);
    setGoodsAtCartCounter(goodsAtCartCounter - prevQty);

    localStorage.setItem("goods", JSON.stringify(goods));
    localStorage.setItem(
      "goodsAtCartCounter",
      JSON.stringify(goodsAtCartCounter - prevQty)
    );
    setOpenModal(false);
  };
  const handleDeleteFromCart = (e, id) => {
    const copiedGoodsArr = goods.map((el) => {
      if (el.EAN === id && parseFloat(el.qtyInOrder) > 1) {
        el.qtyInOrder = parseFloat(el.qtyInOrder) - 1;
      } else if (el.EAN === id && parseFloat(el.qtyInOrder) === 1) {
        el.qtyInOrder = 0;
        el.cart = "false";
      } else {
        return el;
      }
      return el;
    });
    setGoods([...copiedGoodsArr]);
    setGoodsAtCartCounter(goodsAtCartCounter - 1);

    localStorage.setItem("goods", JSON.stringify(goods));
    localStorage.setItem(
      "goodsAtCartCounter",
      JSON.stringify(goodsAtCartCounter - 1)
    );
  };
  const handleModalClose = () => {
    setOpenModal(!openModal);
  };
  const handleAddToCartModal = (
    e,
    id,
    title,
    action,
    direction,
    header,
    text
  ) => {
    setOpenModal(!openModal);
    setModalProps({
      title: title,
      id: id,
      action: action,
      direction: direction,
      text: text,
      header: header,
    });
  };
  const handleAddToCart = (e, id, callFrom) => {
    let counter;
    const findAddedElIndex = goods.map((el) => {
      if (el.EAN === id) {
        switch (el.cart) {
          case "true":
            el.qtyInOrder = parseInt(el.qtyInOrder) + 1;
            counter = goodsAtCartCounter + 1;
            localStorage.setItem("goodsAtCartCounter", counter);
            setGoodsAtCartCounter(counter);
            break;
          case "false":
            el.cart = "true";
            el.qtyInOrder = "1";
            counter = goodsAtCartCounter + 1;
            localStorage.setItem("goodsAtCartCounter", counter);
            setGoodsAtCartCounter(counter);
            break;
          default:
            return null;
        }
        return el;
      } else {
        return el;
      }
    });
    if (callFrom) {
      setGoods([...findAddedElIndex]);
    } else {
      setOpenModal(!openModal);
      setGoods([...findAddedElIndex]);
    }

    localStorage.setItem("goods", JSON.stringify(goods));
  };
  const handleRateClick = (e, id, rate) => {
    const clickedEl = e.target;
    const nextElement = e.target.nextSibling;
    let counter = Number(rate);

    if (
      clickedEl.classList.contains("Card_checked__GyrC1") &&
      !nextElement.classList.contains("Card_checked__GyrC1")
    ) {
      return;
    } else if (!clickedEl.classList.contains("Card_checked__GyrC1")) {
      console.log("ADD");
      clickedEl.classList.add("Card_checked__GyrC1");
      const checkPrevStars = (prevElementNode) => {
        let prevElement = prevElementNode.previousSibling;
        if (prevElement) {
          prevElement.classList.add("Card_checked__GyrC1");
          checkPrevStars(prevElement);
          counter += 1;
        } else {
          return;
        }
      };
      checkPrevStars(clickedEl);
    } else {
      console.log("DEL");
      const checkPrevStars = (prevElementNode) => {
        let prevElement = prevElementNode.nextSibling;
        if (prevElement) {
          prevElement.classList.remove("Card_checked__GyrC1");
          checkPrevStars(prevElement);
          counter -= 1;
        } else {
          return;
        }
      };
      checkPrevStars(clickedEl);
    }
    const findElementIndex = goods.findIndex(({ EAN }) => EAN === id);
    const copiedGoodsArr = goods;
    copiedGoodsArr[findElementIndex].rate = counter;
    console.log(counter);
    setGoods((currentAmount) => {
      localStorage.setItem("goods", JSON.stringify(copiedGoodsArr));
      console.log(localStorage.getItem("goods"));
      return [...copiedGoodsArr];
    });
  };

  useEffect(() => {
    if (localStorage.getItem("goods")) {
      const items = JSON.parse(localStorage.getItem("goods"));
      const goodsAtCartItems = JSON.parse(
        localStorage.getItem("goodsAtCartCounter")
      );
      let inFavoritesItem = localStorage.getItem("inFavorites");
      setGoods([...items]);
      setIsLoaded(true);
      setDisplayCart(false);
      setGoodsAtCartCounter(goodsAtCartItems);
      setInFavorites(inFavoritesItem);
    } else {
      const getData = async () => {
        const goodsArray = await fetch("./items.json").then((res) =>
          res.json()
        );
        setGoods([...goodsArray]);
        setIsLoaded(true);
        setDisplayCart(false);
      };
      getData();
    }
  }, []);

  const { title, action, direction, header, text } = modalProps;
  const goodsAtCart = goodsAtCartCounter;

  return (
    <>
      <Header
        showHideBasket={showHideBasket}
        itemsInCart={goods}
        deleteFromCartClickHandler={handleDeleteFromCart}
        displayCart={displayCart}
        goodsAtCart={goodsAtCart}
        inFavorites={inFavorites}
        handleAddToCart={handleAddToCart}
        handleAddToCartModal={handleAddToCartModal}
      />
      <AppRoutes
        isLoaded={isLoaded}
        handleAddToCartModal={handleAddToCartModal}
        ratesClickHandler={handleRateClick}
        goods={goods}
        openModal={openModal}
        handleModalClose={handleModalClose}
        handleFavoritesClick={handleFavoritesClick}
        inFavorites={inFavorites}
        itemsInCart={goods}
        deleteFromCartClickHandler={handleDeleteFromCart}
        showHideBasket={showHideBasket}
        displayCart={displayCart}
        goodsAtCart={goodsAtCart}
        handleAddToCart={handleAddToCart}
      />
      {openModal && (
        <Modal
          header={header}
          text={
            "Do you really want to " +
            action +
            " " +
            title +
            " " +
            direction +
            " to cart?"
          }
          needCrossBtn={true}
          handleCloseModal={handleCloseModal}
          actions={
            <Button
              text={text}
              handleClick={
                action === "add"
                  ? (e) => handleAddToCart(e, modalProps.id)
                  : (e) => deleteAllItemsFromCart(e, modalProps.id)
              }
            />
          }
        />
      )}
    </>
  );
};

export default App;
