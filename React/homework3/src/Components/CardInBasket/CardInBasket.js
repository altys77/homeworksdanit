import React from "react";
import styles from "./CardInBasket.module.scss";
import crossDeleteBtn from "./img/cross-delete.png";
import classNames from "classnames";
import checkedStyle from "../Card/Card.module.scss";
import PropTypes from "prop-types";
import Button from "../Button/Button";

const BasketCard = (props) => {
  const { checked } = checkedStyle;
  const {
    item,
    qty,
    deleteFromCartClickHandler,
    handleAddToCart,
    id,
    handleAddToCartModal,
  } = props;
  const {
    shoppingBasketWrapper,
    shoppingBasketTitle,
    crossDelete,
    shoppingBasketPrice,
  } = styles;
  return (
    <>
      <div className={shoppingBasketWrapper}>
        <img src={item.image} alt="item_picture"></img>
        <div className={shoppingBasketTitle}>
          <h2>
            <span>{qty + "x"}</span> {item.title + " " + item.color}
          </h2>
          {
            <>
              <span className={classNames("fa fa-star", checked)}></span>
              <span
                className={
                  item.rate >= 1
                    ? classNames("fa fa-star", checked)
                    : classNames("fa fa-star")
                }
              ></span>
              <span
                className={
                  item.rate >= 2
                    ? classNames("fa fa-star", checked)
                    : classNames("fa fa-star")
                }
              ></span>
              <span
                className={
                  item.rate >= 3
                    ? classNames("fa fa-star", checked)
                    : classNames("fa fa-star")
                }
              ></span>
              <span
                className={
                  item.rate >= 4
                    ? classNames("fa fa-star", checked)
                    : classNames("fa fa-star")
                }
              ></span>
            </>
          }
        </div>
        <div>
          <p className={shoppingBasketPrice}>
            {parseFloat(item.price) * item.qtyInOrder + "$"}
          </p>
          <div>
            <Button
              handleClick={(e) => {
                handleAddToCart(e, id, "true");
              }}
              text={"+"}
            />
            <Button
              handleClick={(e) => {
                deleteFromCartClickHandler(e, id);
              }}
              text={"-"}
            />

            <img
              onClick={(e) =>
                handleAddToCartModal(
                  e,
                  id,
                  item.title,
                  "delete all",
                  "from",
                  "Deleting item from cart",
                  "Delete"
                )
              }
              className={crossDelete}
              src={crossDeleteBtn}
              alt="delete-button"
            ></img>
          </div>
        </div>
      </div>
    </>
  );
};

export default BasketCard;

BasketCard.propTypes = {
  item: PropTypes.object,
  qty: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  deleteFromCartClickHandler: PropTypes.func,
};

BasketCard.defaultProps = {
  item: {},
  qty: "",
  deleteFromCartClickHandler: () => {},
};
