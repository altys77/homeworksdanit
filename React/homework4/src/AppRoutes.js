import { Route, Routes } from "react-router-dom";
import Favorites from "./pages/Favorites/Favorites";
import Main from "./Components/Main/Main";
import ShoppingCart from "./pages/ShoppingCart/ShoppingCart";
import { useSelector } from "react-redux";
const AppRoutes = (props) => {
  const isLoaded = useSelector((state) => state.goods.isLoaded);
  return (
    <Routes>
      <Route path="/" element={isLoaded ? <Main /> : <h2>Loading</h2>} />
      <Route path="cart" element={<ShoppingCart />} />
      <Route path="favorites" element={<Favorites />} />
    </Routes>
  );
};

export default AppRoutes;
