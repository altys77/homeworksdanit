import React from "react";
import styles from "./Card.module.scss";
import classNames from "classnames";
import PropTypes from "prop-types";
import { useDispatch } from "react-redux";
import { handleFavoritesClickAC } from "../../store/goods/goods";
import { setInFavoritesAC } from "../../store/inFavorites/inFavorites";
import { setModalOpen, setModalProps } from "../../store/modalProps/modalProps";
import { handleRateClickAC } from "../../store/goods/goods";

const Card = (props) => {
  const {
    item: { title, image, rate, EAN, color, price, inFavorites },
    id,
  } = props;
  const dispatch = useDispatch();
  const { wrapper, checked, favoritesWrapper } = styles;
  return (
    <div className={wrapper}>
      <div className={favoritesWrapper}>
        <img
          onClick={(e) => {
            dispatch(handleFavoritesClickAC(id));
            dispatch(setInFavoritesAC(inFavorites));
          }}
          src={
            inFavorites
              ? "./images/favorites/favorites-checked.png"
              : "./images/favorites/favorite-icon.png"
          }
          alt="fvorites__picture"
        ></img>
      </div>
      <img src={image} alt="card_goods"></img>
      <h2>{title}</h2>
      <div
        onClick={(e) => {
          if (
            e.currentTarget !== e.target &&
            !e.target.classList.contains("Card_checked__zme-Y")
          ) {
            let nextElement = e.target.nextSibling;
            let counter = 5;
            const countElements = (nextElement) => {
              if (nextElement) {
                counter -= 1;
                nextElement = nextElement.nextSibling;
                countElements(nextElement);
              }
            };
            countElements(nextElement);

            dispatch(handleRateClickAC({ id, counter }));
          } else {
            let nextElement = e.target.nextSibling;
            let counter = 0;
            if (!nextElement) {
              return;
            } else {
              const countElements = (nextElement) => {
                if (nextElement.classList.contains("Card_checked__zme-Y")) {
                  counter -= 1;
                  nextElement = nextElement.nextSibling;
                  if (!nextElement) {
                    return;
                  }
                  countElements(nextElement);
                }
              };

              countElements(nextElement);
              const currentRate = Number(rate) + Number(counter);
              dispatch(handleRateClickAC({ id, counter: currentRate }));
            }
          }
        }}
      >
        {
          <>
            <span className={classNames("fa fa-star", checked)}></span>
            <span
              className={
                rate > 1
                  ? classNames("fa fa-star", checked)
                  : classNames("fa fa-star")
              }
            ></span>
            <span
              className={
                rate > 2
                  ? classNames("fa fa-star", checked)
                  : classNames("fa fa-star")
              }
            ></span>
            <span
              className={
                rate > 3
                  ? classNames("fa fa-star", checked)
                  : classNames("fa fa-star")
              }
            ></span>
            <span
              className={
                rate > 4
                  ? classNames("fa fa-star", checked)
                  : classNames("fa fa-star")
              }
            ></span>
          </>
        }
      </div>
      <p>{EAN}</p>
      <p>Description</p>
      <p>{color}</p>
      <div>
        <p>{price}</p>
        <button
          onClick={(e) => {
            dispatch(setModalOpen(true));
            dispatch(
              setModalProps({
                title: title,
                id: id,
                action: "add",
                direction: "to",
                text: "add to cart",
                header: "Adding item to cart",
              })
            );
          }}
        >
          Add to cart
        </button>
      </div>
    </div>
  );
};

export default Card;

Card.propTypes = {
  item: PropTypes.object,
  ratesClickHandler: PropTypes.func,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  handleAddToCartModal: PropTypes.func,
  handleFavoritesClick: PropTypes.func,
};

Card.defaultProps = {
  item: {},
  ratesClickHandler: () => {},
  id: "",
  handleAddToCartModal: () => {},
  handleFavoritesClick: () => {},
};
