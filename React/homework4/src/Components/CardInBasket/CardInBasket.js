import React from "react";
import styles from "./CardInBasket.module.scss";
import crossDeleteBtn from "./img/cross-delete.png";
import classNames from "classnames";
import checkedStyle from "../Card/Card.module.scss";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import { useDispatch } from "react-redux";
import {
  setModalClose,
  setModalOpen,
  setModalProps,
} from "../../store/modalProps/modalProps";
import {
  handleAddToCartAC,
  handleDeleteFromCartAC,
} from "../../store/goods/goods";
import {
  decrementCounter,
  incrementCounter,
} from "../../store/goodsAtCartCounter/goodsAtCartCounter";

const BasketCard = (props) => {
  const { checked } = checkedStyle;
  const dispatch = useDispatch();
  const { item, qty, id } = props;
  const {
    shoppingBasketWrapper,
    shoppingBasketTitle,
    crossDelete,
    shoppingBasketPrice,
  } = styles;
  return (
    <>
      <div className={shoppingBasketWrapper}>
        <img src={item.image} alt="item_picture"></img>
        <div className={shoppingBasketTitle}>
          <h2>
            <span>{qty + "x"}</span> {item.title + " " + item.color}
          </h2>
          {
            <>
              <span className={classNames("fa fa-star", checked)}></span>
              <span
                className={
                  item.rate >= 1
                    ? classNames("fa fa-star", checked)
                    : classNames("fa fa-star")
                }
              ></span>
              <span
                className={
                  item.rate >= 2
                    ? classNames("fa fa-star", checked)
                    : classNames("fa fa-star")
                }
              ></span>
              <span
                className={
                  item.rate >= 3
                    ? classNames("fa fa-star", checked)
                    : classNames("fa fa-star")
                }
              ></span>
              <span
                className={
                  item.rate >= 4
                    ? classNames("fa fa-star", checked)
                    : classNames("fa fa-star")
                }
              ></span>
            </>
          }
        </div>
        <div>
          <p className={shoppingBasketPrice}>
            {parseFloat(item.price) * item.qtyInOrder + "$"}
          </p>
          <div>
            <Button
              handleClick={(e) => {
                dispatch(incrementCounter(1));
                dispatch(handleAddToCartAC(id));
                dispatch(setModalClose(false));
              }}
              text={"+"}
            />
            <Button
              handleClick={(e) => {
                dispatch(handleDeleteFromCartAC(id));
                dispatch(decrementCounter(1));
              }}
              text={"-"}
            />

            <img
              onClick={(e) => {
                dispatch(setModalOpen(true));
                dispatch(
                  setModalProps({
                    title: item.title,
                    id: id,
                    action: "delete all",
                    direction: "from",
                    text: "Delete",
                    header: "Deleting item from cart",
                    qty,
                  })
                );
              }}
              className={crossDelete}
              src={crossDeleteBtn}
              alt="delete-button"
            ></img>
          </div>
        </div>
      </div>
    </>
  );
};

export default BasketCard;

BasketCard.propTypes = {
  item: PropTypes.object,
  qty: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  deleteFromCartClickHandler: PropTypes.func,
};

BasketCard.defaultProps = {
  item: {},
  qty: "",
  deleteFromCartClickHandler: () => {},
};
