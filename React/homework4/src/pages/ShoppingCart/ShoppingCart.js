import styles from "./ShoppingCart.module.scss";
import BasketCard from "../../Components/CardInBasket/CardInBasket";
import { useSelector } from "react-redux";
const ShoppingCart = (props) => {
  const itemsInCart = useSelector((state) => state.goods.goods);
  const goodsAtCart = useSelector((state) => state.goodsAtCartCounter.counter);
  return (
    <>
      <h2>Cart</h2>
      <div className={styles.goodsWrapper}>
        {itemsInCart ? (
          itemsInCart.map(
            (el) =>
              el.cart === true && (
                <BasketCard
                  key={el.EAN}
                  id={el.EAN}
                  item={el}
                  qty={el.qtyInOrder}
                />
              )
          )
        ) : (
          <h2>Loading</h2>
        )}
        {goodsAtCart === 0 && <p>No items has been added yet</p>}
      </div>
    </>
  );
};

export default ShoppingCart;
