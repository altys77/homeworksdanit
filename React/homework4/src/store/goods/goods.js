import { createSlice, current } from "@reduxjs/toolkit";
import getGoods from "./ASYNCgetGoods/getGoods";

const goodsReducer = createSlice({
  name: "goods",
  initialState: {
    goods: [],
    isLoaded: false,
  },
  reducers: {
    handleLoading: (state, action) => {
      state.isLoaded = action.payload;
    },
    handleFavoritesClickAC: (state, action) => {
      const elementIndex = current(state.goods).findIndex(
        ({ EAN }) => EAN === action.payload
      );
      if (current(state.goods)[elementIndex].inFavorites) {
        state.goods[elementIndex].inFavorites = false;
      } else {
        state.goods[elementIndex].inFavorites = true;
      }
      localStorage.setItem("goods", JSON.stringify(state.goods));
    },
    deleteAllItemsFromCartAC: (state, action) => {
      const findElIndex = state.goods.findIndex(
        ({ EAN }) => EAN === action.payload
      );
      state.goods[findElIndex].qtyInOrder = 0;
      state.goods[findElIndex].cart = false;
      localStorage.setItem("goods", JSON.stringify(state.goods));
    },
    handleDeleteFromCartAC: (state, action) => {
      const findElementIndex = state.goods.findIndex(
        ({ EAN }) => EAN === action.payload
      );
      const elQty = state.goods[findElementIndex].qtyInOrder;
      console.log(elQty);
      if (elQty > 1) {
        state.goods[findElementIndex].qtyInOrder = elQty - 1;
      } else if (elQty === 1) {
        state.goods[findElementIndex].qtyInOrder = 0;
        state.goods[findElementIndex].cart = false;
      }
      localStorage.setItem("goods", JSON.stringify(state.goods));
    },
    handleAddToCartAC: (state, action) => {
      const findElement = current(state.goods).findIndex(
        (el) => el.EAN === action.payload
      );
      if (current(state.goods[findElement]).cart === true) {
        state.goods[findElement].qtyInOrder =
          parseInt(state.goods[findElement].qtyInOrder) + 1;
      } else {
        state.goods[findElement].cart = true;
        state.goods[findElement].qtyInOrder = 1;
      }
      localStorage.setItem("goods", JSON.stringify(state.goods));
    },
    handleRateClickAC: (state, action) => {
      const findElement = current(state.goods).findIndex(
        (el) => el.EAN === action.payload.id
      );
      state.goods[findElement].rate = action.payload.counter;
      localStorage.setItem("goods", JSON.stringify(state.goods));
    },
    setGoodsFromLSAC: (state, action) => {
      state.goods = action.payload;
    },
  },
  extraReducers: {
    [getGoods.fulfilled]: (state, action) => {
      state.goods = action.payload;
      state.isLoaded = true;
    },
  },
});

export default goodsReducer.reducer;
export const {
  extraReducers,
  handleFavoritesClickAC,
  deleteAllItemsFromCartAC,
  handleDeleteFromCartAC,
  handleAddToCartAC,
  handleRateClickAC,
  setGoodsFromLSAC,
  handleLoading,
} = goodsReducer.actions;
