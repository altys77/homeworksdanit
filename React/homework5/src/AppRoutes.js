import { Route, Routes } from "react-router-dom";
import Favorites from "./pages/Favorites/Favorites";
import Main from "./Components/Main/Main";
import ShoppingCart from "./pages/ShoppingCart/ShoppingCart";
import { useSelector } from "react-redux";
import CompletedOrder from "./pages/OrderCompleted/OrderCompleted";
const AppRoutes = () => {
  const isLoaded = useSelector((state) => state.goods.isLoaded);
  const isOrderSuccessful = useSelector(
    (state) => state.goods.isOrderSuccessful
  );

  return (
    <Routes>
      <Route path="/" element={isLoaded ? <Main /> : <h2>Loading</h2>} />
      <Route path="cart" element={<ShoppingCart />} />
      <Route path="favorites" element={<Favorites />} />
      <Route
        path="successfulOrder"
        element={
          isOrderSuccessful ? <CompletedOrder /> : <h2>Order not found</h2>
        }
      />
    </Routes>
  );
};

export default AppRoutes;
