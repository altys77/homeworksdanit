import { useField } from "formik";
import styles from "./CustomForm.module.scss";

const CustomForm = (props) => {
  const { type, placeholder } = props;

  const [field, meta] = useField(props);
  return (
    <div className={styles.formWrapper}>
      <div>
        <label
          htmlFor={field.name}
        >{`${field.name.toLocaleUpperCase()}: `}</label>
      </div>
      <div>
        <input
          name={field.name}
          type={type}
          placeholder={placeholder}
          value={field.value}
          onChange={field.onChange}
          onBlur={field.onBlur}
        />
        {meta.error && meta.touched && (
          <span style={{ color: "red", display: "block" }}>{meta.error}</span>
        )}
      </div>
    </div>
  );
};

export default CustomForm;
