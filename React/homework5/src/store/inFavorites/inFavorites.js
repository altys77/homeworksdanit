import { createSlice } from "@reduxjs/toolkit";

const inFavoritesReducer = createSlice({
  name: "inFavorites",
  initialState: {
    inFavorites: 0,
  },
  reducers: {
    setInFavoritesFromServerAc: (state, action) => {
      state.inFavorites = Number(action.payload);
    },
    setInFavoritesAC: (state, action) => {
      if (action.payload) {
        state.inFavorites = state.inFavorites - 1;
        localStorage.setItem("inFavorites", state.inFavorites);
      } else {
        state.inFavorites = state.inFavorites + 1;
        localStorage.setItem("inFavorites", state.inFavorites);
      }
    },
  },
});

export default inFavoritesReducer.reducer;
export const { setInFavoritesAC, setInFavoritesFromServerAc } =
  inFavoritesReducer.actions;
