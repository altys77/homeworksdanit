import { createSlice } from "@reduxjs/toolkit";

const modalPropsReducer = createSlice({
  name: "modalProps",
  initialState: {
    modalProps: {},
    isOpen: false,
  },
  reducers: {
    setModalProps: (state, action) => {
      state.modalProps = action.payload;
    },
    setModalOpen: (state, action) => {
      state.isOpen = action.payload;
    },
    setModalClose: (state, action) => {
      state.isOpen = action.payload;
    },
  },
});

export default modalPropsReducer.reducer;
export const { setModalProps, setModalClose, setModalOpen } =
  modalPropsReducer.actions;
