import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import store from "../../store/index";
import { BrowserRouter } from "react-router-dom";
import Card from "./Card";

// const {
//     item: { title, image, rate, EAN, color, price, inFavorites },
//     id,
//   } = props;

const Component = () => {
  return (
    <Card
      itme={{
        title: "Iphone",
        image: "#",
        rate: 4,
        EAN: 123,
        color: "blue",
        price: "1200$",
        inFavorites: true,
      }}
      id={"123"}
    />
  );
};

const MockComponent = () => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Component />
      </BrowserRouter>
    </Provider>
  );
};

describe("Card renders", () => {
  test("should Card render items", () => {
    const { asFragment } = render(<MockComponent />);

    expect(asFragment()).toMatchSnapshot();
  });
});
