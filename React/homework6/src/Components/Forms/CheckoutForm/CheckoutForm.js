import { Formik, Form } from "formik";
import initialValues from "./initialValues";
import CustomForm from "./CustomForm/CustomForm";
import validationShema from "./validationShema";
import styles from "./CheckoutForm.module.scss";
import { handleCheckout, setOrderStatus } from "../../../store/goods/goods";
import { useDispatch } from "react-redux";
import { setCheckoutValue } from "../../../store/goodsAtCartCounter/goodsAtCartCounter";
import { useNavigate } from "react-router-dom";

const CheckOutForm = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationShema}
      onSubmit={(value) => {
        dispatch(handleCheckout(value));
        dispatch(setCheckoutValue(0));
        dispatch(setOrderStatus(true));
        navigate("/successfulOrder");
      }}
    >
      {(props) => {
        return (
          <Form className={styles.checkoutWrapper}>
            <CustomForm type="text" placeholder="name" name="name" />
            <CustomForm type="text" placeholder="lastname" name="lastName" />
            <CustomForm type="text" placeholder="age" name="age" />
            <CustomForm type="text" placeholder="address" name="address" />
            <CustomForm type="text" placeholder="tel" name="tel" />
            <button disabled={props.isValid ? false : true} type="submit">
              Submit
            </button>
          </Form>
        );
      }}
    </Formik>
  );
};

export default CheckOutForm;
