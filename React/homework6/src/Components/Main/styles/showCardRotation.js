import React from "react";

const rotation = {
  table: {
    display: "grid",
    "grid-template-columns": "repeat(1, minmax(320px, 1fr))",
    padding: "0px 100px 0px 100px",
    "align-items": "center",
    "justify-items": "center",
    gap: "40px",
  },
  cards: {
    display: "grid",
    "grid-template-columns": "repeat(auto-fit, minmax(320px, 1fr))",
    padding: "0px 100px 0px 100px",
    "align-items": "center",
    "justify-items": "center",
    gap: "40px",
  },
};

const ThemeContext = React.createContext(rotation);

export default ThemeContext;
