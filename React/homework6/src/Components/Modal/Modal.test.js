import { screen, render, fireEvent } from "@testing-library/react";
import Modal from "./Modal.";
import { Provider, useDispatch, useSelector } from "react-redux";
import store from "../../store/index";
import modalProps, {
  setModalOpen,
  setModalClose,
} from "../../store/modalProps/modalProps";

const Component = () => {
  const dispatch = useDispatch();
  const isOpen = useSelector((state) => state.modalProps.isOpen);
  return (
    <>
      <button onClick={() => dispatch(setModalOpen(true))}>OPEN</button>
      {isOpen && (
        <Modal
          text={"Modal Text"}
          header={"Modal Header"}
          needCrossBtn={true}
          actions={
            <button onClick={() => dispatch(setModalClose(false))}>
              CLOSE
            </button>
          }
        />
      )}
    </>
  );
};

const MockProvider = (hasToBeRendered) => {
  return (
    <Provider store={store}>
      <Component />
    </Provider>
  );
};

describe("Modal has to be rendered", () => {
  test("should modal render", () => {
    render(<MockProvider />);
    const openBtn = screen.getByText("OPEN");
    fireEvent.click(openBtn);
    expect(screen.getByTestId("modal-root")).toBeInTheDocument();
  });
  test("should modal close at btn click", () => {
    render(<MockProvider />);
    const openBtn = screen.getByText("OPEN");
    fireEvent.click(openBtn);
    const closeBtn = screen.getByText("CLOSE");
    fireEvent.click(closeBtn);
    expect(screen.queryByTestId("modal-root")).not.toBeInTheDocument();
  });
  test("should modal close at background click", () => {
    render(<MockProvider />);
    const openBtn = screen.getByText("OPEN");
    fireEvent.click(openBtn);
    const closeBackground = screen.getByTestId("modal-root");
    fireEvent.click(closeBackground);
    expect(screen.queryByTestId("modal-root")).not.toBeInTheDocument();
  });
});
