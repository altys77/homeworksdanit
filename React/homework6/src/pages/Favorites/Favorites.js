import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Card from "../../Components/Card/Card";
import { setIsClose } from "../../store/shoppingCart/shoppingCart";
import styles from "./Favorites.module.scss";

const Favorites = (props) => {
  const goods = useSelector((state) => state.goods.goods);
  const goodsAtFavorites = goods.filter((el) => el.inFavorites);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setIsClose());
  }, []);
  return (
    <div className={styles.sectionWrapper}>
      <h2 style={{ textAlign: "center", margin: "20px" }}>Favorites</h2>
      <div className={styles.goodsWrapper}>
        {goodsAtFavorites.length === 0
          ? "No items has been added"
          : goodsAtFavorites.map((el) => (
              <Card item={el} key={el.EAN} id={el.EAN} />
            ))}
      </div>
    </div>
  );
};

export default Favorites;
