import { createAsyncThunk } from "@reduxjs/toolkit";
import { handleLoading } from "../goods";
import { setGoodsAtCartCounter } from "../../goodsAtCartCounter/goodsAtCartCounter";
import { setInFavoritesFromServerAc } from "../../inFavorites/inFavorites";

const getGoods = createAsyncThunk("goods/getGoods", async (_, { dispatch }) => {
  if (localStorage.getItem("goods")) {
    const items = JSON.parse(localStorage.getItem("goods"));
    const goodsAtCartItems = JSON.parse(
      localStorage.getItem("goodsAtCartCounter")
    );
    let inFavoritesItem = localStorage.getItem("inFavorites");
    dispatch(handleLoading(true));

    dispatch(setGoodsAtCartCounter(Number(goodsAtCartItems)));
    dispatch(setInFavoritesFromServerAc(inFavoritesItem));
    return [...items];
  } else {
    const goodsArray = await fetch("./items.json").then((res) => res.json());

    return goodsArray;
  }
});
export default getGoods;
