import { setOrderStatus, handleLoading, setGoodsFromLSAC } from "./goods";

import goodsReducer from "./goods";

const initState = {
  goods: [],
  isLoaded: false,
  isOrderSuccessful: false,
  orderedGoodsData: [],
};

describe("Goods reducers", () => {
  test("should return the initial state", () => {
    expect(goodsReducer(undefined, { type: undefined })).toEqual({
      goods: [],
      isLoaded: false,
      isOrderSuccessful: false,
      orderedGoodsData: [],
    });
  });
  test("should setOrderStatus return true", () => {
    expect(
      goodsReducer(initState, { type: setOrderStatus, payload: true })
    ).toEqual({
      goods: [],
      isLoaded: false,
      isOrderSuccessful: true,
      orderedGoodsData: [],
    });
  });
  test("should handleLoading return true", () => {
    expect(
      goodsReducer(initState, { type: handleLoading, payload: true })
    ).toEqual({
      goods: [],
      isLoaded: true,
      isOrderSuccessful: false,
      orderedGoodsData: [],
    });
  });
  test("should setGoodsFromLSAC set the goods array", () => {
    expect(
      goodsReducer(initState, {
        type: setGoodsFromLSAC,
        payload: [{ title: "Iphone", EAN: 123, color: "black" }],
      })
    ).toEqual({
      goods: [{ title: "Iphone", EAN: 123, color: "black" }],
      isLoaded: false,
      isOrderSuccessful: false,
      orderedGoodsData: [],
    });
  });
});
