import goodsAtCartCounter, { setCheckoutValue } from "./goodsAtCartCounter";
import {
  incrementCounter,
  decrementCounter,
  setGoodsAtCartCounter,
} from "./goodsAtCartCounter";

const initialState = {
  counter: 0,
};

describe("goodsAtCartCounter works", () => {
  test("should return the initial state", () => {
    expect(goodsAtCartCounter(undefined, { type: undefined })).toEqual(
      initialState
    );
  });
  test("should return the state + 1", () => {
    expect(
      goodsAtCartCounter(initialState, { type: incrementCounter, payload: 1 })
    ).toEqual({
      counter: 1,
    });
  });
  test("should return the state - 1", () => {
    expect(
      goodsAtCartCounter(initialState, { type: decrementCounter, payload: 1 })
    ).toEqual({
      counter: -1,
    });
  });
  test("should return the state 15", () => {
    expect(
      goodsAtCartCounter(initialState, {
        type: setGoodsAtCartCounter,
        payload: 15,
      })
    ).toEqual({
      counter: 15,
    });
  });
  test("should return the state 100", () => {
    expect(
      goodsAtCartCounter(initialState, { type: setCheckoutValue, payload: 100 })
    ).toEqual({
      counter: 100,
    });
  });
});
