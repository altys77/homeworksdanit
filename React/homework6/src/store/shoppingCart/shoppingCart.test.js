import { setIsOpen, setIsClose } from "./shoppingCart";
import headerCartReducer from "./shoppingCart";

const initialState = {
  isOpen: false,
};

describe("Shopping Cart works", () => {
  test("should shopping cart init", () => {
    expect(headerCartReducer(undefined, { type: undefined })).toEqual({
      isOpen: false,
    });
  });
  test("should shopping cart open", () => {
    expect(headerCartReducer(initialState, { type: setIsOpen })).toEqual({
      isOpen: true,
    });
  });
});
