import { memo } from "react";
import emptyAvatar from "../../images/EmptyAvatar/emptyAvatar.svg";

const AvatarImage = (props) => {
  const { url } = props;
  return <img src={url ? url : emptyAvatar} alt="user-avatar" />;
};

export default memo(AvatarImage);
